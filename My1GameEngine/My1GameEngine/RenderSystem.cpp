#include "RenderSystem.h"

int RenderSystem::SetupGraphicsConext()
{
	// 1) initialize glfw
	if (!glfwInit()) // Initialize the glfw library
	{
		return -1;
	}
	// tell to GLFW  that our openGL version is 3.3; and we want to use core profile
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	_window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
	if (!_window)
	{
		glfwTerminate();
		return -2;
	}
	/* Make the window's context current */
	glfwMakeContextCurrent(_window);

	// 2) initialize glad
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		return -3;
	}
	glfwSetFramebufferSizeCallback(_window, framebuffer_size_callback);
}

int RenderSystem::StartLoop()
{
	//render loop
	while (!glfwWindowShouldClose(_window))
	{
		// set up glad; set up glfw
		glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		glfwSwapBuffers(_window);
		glfwPollEvents();
	}
	glfwTerminate();
	return 0;
}

unsigned int RenderSystem::LoadMesh(float* Data, int SizeOfData)
{
	unsigned int VBO; // unique ID
	glGenBuffers(1, &VBO); // ask OpenGL to create ID to new Vertex Buffer Object
	glBindBuffer(GL_ARRAY_BUFFER, VBO); // declare buffer type and make VBO current
	glBufferData(GL_ARRAY_BUFFER, SizeOfData, Data, GL_STATIC_DRAW);
	return 0;
}

RenderSystem::RenderSystem()
{
	_window = NULL;
	SetupGraphicsConext();
	StartLoop();
}
void RenderSystem::framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}

