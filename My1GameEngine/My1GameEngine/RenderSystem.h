#pragma once
#include <glad/glad.h>
#include <GLFW/glfw3.h>
/*
	RenderSystem class that execute openGL commands.
	You should send any graphics data and shaders
	to this system
*/
class RenderSystem
{
	public:
		RenderSystem();
		int SetupGraphicsConext();
		int StartLoop();
		unsigned int LoadMesh(float* Data, int SizeOfData);
	private:
		GLFWwindow* _window;
		static void  framebuffer_size_callback(GLFWwindow* window, int width, int height);
};

